#########################################################################
# Base Container
##########################################################################

FROM alpine:3.4 AS base-env

ENV TOOL_HOME /root/translate/
WORKDIR $TOOL_HOME

# Install Java
RUN apk --update add openjdk8-jre-base && \
    rm -rf /var/cache/apk/*

##########################################################################
# Build Container
##########################################################################

FROM base-env AS build-env

RUN apk --update add wget ca-certificates bash openjdk8

# Install Leiningen
ENV LEIN_INSTALL=/usr/local/bin/
ENV PATH=$PATH:$LEIN_INSTALL
ENV LEIN_ROOT true
RUN wget -q "https://raw.githubusercontent.com/technomancy/leiningen/stable/bin/lein" \
         -O $LEIN_INSTALL/lein && \
    chmod +x $LEIN_INSTALL/lein && \
    lein

# Deploy service code
ADD . $TOOL_HOME

# Build the jar file
RUN lein uberjar

##########################################################################
# Final Container
##########################################################################

FROM base-env

COPY --from=build-env $TOOL_HOME/target/translate.jar $TOOL_HOME
COPY scripts/entrypoint.sh $TOOL_HOME

ENTRYPOINT ["/root/translate/entrypoint.sh"]
