(ns translate.core
  (:gen-class)
  (:require [clojure.string :as string]
            [clojure.spec.alpha :as s]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Helpers
;;
(defn long-str [& strings] (clojure.string/join strings))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Constants
;;
(def ^:const ZERO (long 0))
(def ^:const ONE (long 1))
(def ^:const TEN (long 10))
(def ^:const ONE_HUNDRED (long 100))
(def ^:const ONE_THOUSAND (long 1000))

(def ^:const power-of-thousand-words
  ["" "thousand" "million" "billion" "trillion" "quadrillion" "quintillion"])

(def ^:const min-value-translation
  (long-str
   "minus nine quintillion, two-hundred and twenty-three quadrillion, "
   "three-hundred and seventy-two trillion, thirty-six billion, eight-hundred "
   "and fifty-four million, seven-hundred and seventy-five thousand, "
   "eight-hundred and eight"))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Internals
;;
(defn- beautify
  "Capitalise and add a full-stop."
  [translation]
  (str (string/capitalize translation) "."))


(defn- translate-units
  "Simple look-up of units as words"
  [pos-digit]
  (case (long pos-digit)
    1 "one"
    2 "two"
    3 "three"
    4 "four"
    5 "five"
    6 "six"
    7 "seven"
    8 "eight"
    9 "nine"))


(defn- translate-tens
  "Look-up translation for a given number of 10s"
  [count]
  (case (long count)
    1 "ten"
    2 "twenty"
    3 "thirty"
    4 "fourty"
    5 "fifty"
    6 "sixty"
    7 "seventy"
    8 "eighty"
    9 "ninety"))


(defn- translate-teens
  "Look-up translation for numbers between 10 and 19"
  [count]
  (case (long count)
    1 "eleven"
    2 "twelve"
    3 "thirteen"
    4 "fourteen"
    5 "fifteen"
    6 "sixteen"
    7 "seventeen"
    8 "eighteen"
    9 "nineteen"))


(defn- translate-hundreds
  "Translation for a number of 100s"
  [count]
  (str (translate-units count) "-hundred"))


(defn- translate-group
  "Translate a power-of-thousand group 0-999 * (1000)^n"
  [hundreds tens units power-of-thousand
   translated-so-far left-to-translate]
  (str (if (pos? hundreds)
                  (str (translate-hundreds hundreds))
                  (when (and (pos? left-to-translate)
                             (not (string/includes? translated-so-far " and "))
                             (or (pos? tens) (pos? units))) " and "))

                (str (when (and (pos? hundreds)
                                (or (pos? tens) (pos? units))) " and ")
                     (when (> tens 1)
                       (translate-tens tens))

                     (if (= tens 1)
                       (if (pos? units)
                         (translate-teens units)
                         "ten")
                       (when (pos? units)
                         (str (when (pos? tens) "-")
                              (translate-units units)))))

                (when (and (pos? power-of-thousand)
                           (or (pos? hundreds)
                               (pos? tens)
                               (pos? units)))
                  (str " " (nth power-of-thousand-words power-of-thousand))
                  )))


(defn- compile-translation
  "Translate numbers in powers of 1000."
  ([number]
   (if (neg? number)
     (if (= number (Long/MIN_VALUE))
       min-value-translation
       (str "minus " (compile-translation (- number) "" ZERO)))
     (compile-translation number "" ZERO)))

  ([number translated-so-far power-of-thousand]
   (if (zero? number)
     (if (empty? translated-so-far) "zero" translated-so-far) ;; Base case

     (let [to-translate (mod number ONE_THOUSAND)
           left-to-translate (quot number ONE_THOUSAND)

           hundreds (quot to-translate ONE_HUNDRED)
           tens (quot (- to-translate
                         (* hundreds ONE_HUNDRED)) TEN)
           units (quot (- to-translate
                          (* hundreds ONE_HUNDRED)
                          (* tens TEN)) ONE)

           group-translation
           (translate-group hundreds tens units power-of-thousand
                            translated-so-far left-to-translate)
           ]

       (recur left-to-translate
              (str group-translation
                   (when (and (not-empty translated-so-far)
                              (not (string/starts-with? translated-so-far
                                                        " and "))) ", ")
                   translated-so-far)
              (inc power-of-thousand))))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Public API
;;
(defn translate
  "Translates number parameter into a grammatically correct English expression."
  [number]
  (-> number
      compile-translation
      beautify))

(s/fdef ::translate
  :args (s/cat :number pos-int?)
  :ret (s/and string? seq))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; CLI harness
;;
(defn- usage
  "A helpful usage note."
  []
  (string/join
   \newline
   ["Translate numbers into grammatically correct English expressions."
    ""
    "Usage: translate.sh <number>"
    ""]))


(defn -main
  [& args]
  ;; Lazily translate from stdin when no args passed - only via translate.sh
  (if (zero? (count args))
    (with-open [rdr (java.io.BufferedReader. *in*)]
      (doall (map #(try
                     (println (translate(Long/valueOf
                                         (string/trim (str %)))))
                     (catch Exception _)) (line-seq rdr))))

    (if-not (= 1 (count args))
      (println (usage))
      (try
        (println (translate (Long/valueOf (str (first args)))))

        (catch java.lang.NumberFormatException _
          (println (str "Number must be within ["
                        Long/MIN_VALUE " - " Long/MAX_VALUE "]")))

        (catch Exception e
          (println (usage))
          )))))
