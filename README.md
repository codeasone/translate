# translate

A Clojure library and CLI tool designed to translate numbers into grammatically correct English expressions.

## Usage

### Local (requires lein)

    ./translate.sh <number>

### Streamed

Numbers are translated as entered ad-infinitim:

    ./translate.sh <RET>

### Batch

```
➜  translate git:(master) ✗ ./translate.sh < fixtures/numbers.txt
Six.
One thousand.
Ninety-nine.
One million, ten thousand, one-hundred and one.
Sixty-six.
```

### Docker (no system polution)

    docker build -t translate .
    docker run translate <number>

### Library

```clojure
(ns yours.core
  (:require [translate.core :refer translate]))

(translate 100) ;;=> "One hundred."
```

## Limitations

- `<number>` must be within the range of `Long` (64-bit signed integer)

## Development

### Prerequisites

- [Leiningen](https://leiningen.org)

### Running unit tests

    lein test

### Coverage

    lein cloverage

    |----------------+---------+---------|
    |      Namespace | % Forms | % Lines |
    |----------------+---------+---------|
    | translate.core |   79.44 |   82.35 |
    |----------------+---------+---------|
    |      ALL FILES |   79.44 |   82.35 |
    |----------------+---------+---------|

__Note:__ 100% coverage of core API and internals achieved.

## License

Copyright © 2018 Mark Stuart
