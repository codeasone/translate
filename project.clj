(defproject translate "0.1.0-SNAPSHOT"
  :description "A Clojure library and tool designed to translate numbers into grammatically correct English expressions."
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [org.clojure/test.check "0.10.0-alpha2"]]
  :main translate.core
  :uberjar-name "translate.jar"
  :profiles
  {:dev {:global-vars {*warn-on-reflection* false}
         :plugins [[lein-cloverage "1.0.9"]
                   [lein-kibit "0.1.6"]]}
   :uberjar {:aot :all}})
