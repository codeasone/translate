(ns translate.core-test
  (:require [clojure.test :refer :all]
            [clojure.spec.test.alpha :as st]
            [translate.core :as unit :refer :all]))

;; 10000 generative tests just to gauge robustness
(deftest translation-robustness
  (is (= true (-> (st/check-fn unit/translate
                               ::unit/translate
                               {:clojure.spec.test.check/opts
                                {:num-tests 10000}})
                  :clojure.spec.test.check/ret
                  :result))))

;; Example-based tests
(deftest zero
  (is (= (translate 0) "Zero.")))

(deftest one
  (is (= (translate 1) "One.")))

(deftest minus-one
  (is (= (translate -1) "Minus one.")))

(deftest five
  (is (= (translate 5) "Five.")))

(deftest ten
  (is (= (translate 10) "Ten.")))

(deftest eleven
  (is (= (translate 11) "Eleven.")))

(deftest twelve
  (is (= (translate 12) "Twelve.")))

(deftest thirteen
  (is (= (translate 13) "Thirteen.")))

(deftest nineteen
  (is (= (translate 19) "Nineteen.")))

(deftest twenty
  (is (= (translate 20) "Twenty.")))

(deftest twenty-one
  (is (= (translate 21) "Twenty-one.")))

(deftest ninety
  (is (= (translate 90) "Ninety.")))

(deftest ninety-nine
  (is (= (translate 99) "Ninety-nine.")))

(deftest one-hundred
  (is (= (translate 100) "One-hundred.")))

(deftest one-hundred-and-ten
  (is (= (translate 110) "One-hundred and ten.")))

(deftest one-hundred-and-eleven
  (is (= (translate 111) "One-hundred and eleven.")))

(deftest one-hundred-and-thirty-one
  (is (= (translate 131) "One-hundred and thirty-one.")))

(deftest nine-hundred
  (is (= (translate 900) "Nine-hundred.")))

(deftest nine-hundred-and-nine
  (is (= (translate 909) "Nine-hundred and nine.")))

(deftest nine-hundred-and-nine
  (is (= (translate 999) "Nine-hundred and ninety-nine.")))

(deftest one-thousand
  (is (= (translate 1000) "One thousand.")))

(deftest one-thousand-and-one
  (is (= (translate 1001) "One thousand and one.")))

(deftest one-thousand-and-thirty-one
  (is (= (translate 1031) "One thousand and thirty-one.")))

(deftest one-thousand-one-hundred-and-thirty-one
  (is (= (translate 1131) "One thousand, one-hundred and thirty-one.")))

(deftest ten-thousand-and-thirty-one
  (is (= (translate 10031) "Ten thousand and thirty-one.")))

(deftest eleven-thousand-and-thirty-one
  (is (= (translate 11031) "Eleven thousand and thirty-one.")))

(deftest one-hundred-and-ten-thousand-and-thirty-one
  (is (= (translate 110031) "One-hundred and ten thousand and thirty-one.")))

(deftest one-million
  (is (= (translate 1000000) "One million.")))

(deftest one-million-and-thirty-one
  (is (= (translate 1000031) "One million and thirty-one.")))

(deftest one-million-twenty-thousand-and-thirty-one
  (is (= (translate 1020031) "One million, twenty thousand and thirty-one.")))

(deftest one-million-one-hundred-thousand-and-thirty-one
  (is (= (translate 1100031) "One million, one-hundred thousand and thirty-one.")))

(deftest eleven-million-and-thirty-one
  (is (= (translate 11000031) "Eleven million and thirty-one.")))

(deftest in-words-999000031
  (is (= (translate 999000031) "Nine-hundred and ninety-nine million and thirty-one.")))

(deftest in-words-999999931
  (is (= (translate 999999931) "Nine-hundred and ninety-nine million, nine-hundred and ninety-nine thousand, nine-hundred and thirty-one.")))

(deftest one-billion
  (is (= (translate 1000000000) "One billion.")))

(deftest one-billion-and-thirty-one
  (is (= (translate 1000000031) "One billion and thirty-one.")))

(deftest in-words-10999999931
  (is (= (translate 10999999931) "Ten billion, nine-hundred and ninety-nine million, nine-hundred and ninety-nine thousand, nine-hundred and thirty-one.")))

(deftest one-trillion
  (is (= (translate 1000000000000) "One trillion.")))

(deftest one-trillion-and-thirty-one
  (is (= (translate 1000000000031) "One trillion and thirty-one.")))

(deftest max-long
  (is (= (translate Long/MAX_VALUE) "Nine quintillion, two-hundred and twenty-three quadrillion, three-hundred and seventy-two trillion, thirty-six billion, eight-hundred and fifty-four million, seven-hundred and seventy-five thousand, eight-hundred and seven.")))

(deftest min-long
  (is (= (translate Long/MIN_VALUE) "Minus nine quintillion, two-hundred and twenty-three quadrillion, three-hundred and seventy-two trillion, thirty-six billion, eight-hundred and fifty-four million, seven-hundred and seventy-five thousand, eight-hundred and eight.")))
